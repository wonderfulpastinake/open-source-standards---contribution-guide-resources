Open Source Standards – Update Policy
===

[TOC]

# Terms and Definitions

## Consortium
- group of people working on the official new release of the standard
- consists of at least 1 representative of each [Stakeholder Category](https://pad2.opensourceecology.de/0mVLXPfWTaiFSaSWNYFLIA#Stakeholder-category)
- includes at least one representative of a [Conservative Force](https://pad2.opensourceecology.de/0mVLXPfWTaiFSaSWNYFLIA#Conservative-Force-wants-to-preserve)
- exists for a limited amount of time
- every member in the consortium has one vote per decision
- decisions are made with a full [Consent](https://pad2.opensourceecology.de/0mVLXPfWTaiFSaSWNYFLIA#Consent) except where otherwise stated
- is represented by a consortuium leader and a consortium co-leader

## Stakeholder Category
- represents a group of people affected by the standard (e.g. OSH developers, OSH collaboration platform)
- share a common perspective on the standard (e.g. work with it in a certain way)
- defined by the consortium (stakeholder categories may change with the evolution of the standard)

## Conservative Force (wants to preserve)
- has been part of a previous consortium
- probably carries the philosophy of previous consortiums and is aware of old goals
- may rather vote and argue against changes
- is
	- former consortium member of one of the last 2 official standard releases 
and/or
	- representative of an organisation maintaining this standard (e.g. DIN e.V.) 
	
## Transformative Force (wants to change)
- brings in a new perspective
- may rather vote and argue in favour for changes
- is probably not aware of all backgrounds of decisions made in previous consortiums
- is either
	- a new representative of a previous stakeholder category
or
	- a new representative of a new stakeholder category
	
## Consent
- consent (no one votes _agains_ it) ≠ consensus (everyone votes _for_ it)
- a vote can be
	- positive (no disagreement) → alright!
	- emotionally/vaguely negative (no specified reason, just a feeling) → should be noticed
	- negative (for a specified reason) → needs to be discussed (…and resolved, when a full consent is required)

# The Rules 

## § 0 Submission to Licensing Terms and Local Law
Pretty self-explanatory.
"Licensing terms" refer to the license under which the standard is published.

## § 1 General transparency
All relevant actions need to be tracked and published so that anyone (regardless of the practical effort) may reproduce all decisions made by the consortium.

## § 2 General openness
1. Everyone is invited for participation. The consortium remains open until the final approval of the new version of the standard. 

2. Joining the consortium is free of charge and requirements except the following. A new member:
	1. commits to active contribution and explains to the consortium that he/she has the resources for this
	2. submits to the use of workflows and tools which the consortium previously agreed on
	3. needs the acceptance of the existent consortium. The consortium decides in a meeting, in which the potential new member is present, about the accession.
	4. is representative of a stakeholder category
	5. represents either a transformative or conservative force

## § 3 Founding a new Consortium
1. Given that there is a practical need to officially update the standard, someone organises an initial meeting of the new Consortium (kickoff). 
2. All potential Conservative Forces need to be notified about the founding of a new Consortium. This needs to be proven (e.g. by a server status notification with a successful mail delivery). Note that there's no need to prove that the notification has reached its recipient.
3. During the kickoff, attendees may commit to the active contribution for the new release of the standard. 
4. This forms the new Consortium if the requirements stated in the [Definitions](https://pad2.opensourceecology.de/0mVLXPfWTaiFSaSWNYFLIA#Terms-and-Definitions) above are met. If not, The potential new Consortium either
	1. decides about a change of the stakeholder categories so that the requirements are met (given that none of the potential Conservative Forces blocks this)
or
	2. creates a fork of the standard instead of an update
or
	3. disbands.

## § 5 Public feedback loop
A draft can only be published as the new official release of the standard if
1. it was for at least 4 weeks possible for the general public to give feedback
2. the consortium has made decisions on every feedback given.

## § 6 In case of conflicts
1. Members can be permanentaly excluded from the Consortium by a majority decision of $\geq \frac{2}{3}$ of all members.
2. Excluded members cannot join this Consortium again.
3. Excluded members don't qualify as a Conservative Force for future Consortiums.

# Pracical cases

## OKH metadata standard

### stakeholder categories
- OSH developer
- OSH upload platform
- OSH search engine

## DIN SPEC 3105

## OSH Guideline

